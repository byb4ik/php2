<?php

namespace App;

class View
{
    protected $data = [];

    public function assign($name, $value)
    {
        $this->data[$name] = $value;

        return $this;
    }

    public function render($template)
    {
        ob_start();
        foreach ($this->data as $key => $value) {
            $$key = $value;
        }

        require $template;

        return ob_get_clean();
    }

    public function display($template)
    {
        echo $this->render($template);
    }

}