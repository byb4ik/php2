<?php


namespace App;


class Config
{
    use traitSingleton;

    public $data = [];

    private function __construct()
    {
        $this->data = require __DIR__ . '/cfg.php';
    }
}






