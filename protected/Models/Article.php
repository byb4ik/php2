<?php

namespace App\Models;

use App\Model;

/**
 * @property string $title
 * @property string $text
 */
class Article extends Model
{
    protected static $table = 'articles';

    public $title;
    public $text;

}
