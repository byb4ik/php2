<?php

namespace App;

trait traitSingleton
{
    private static $instance;

    private function __construct() { /* ... @return Singleton */ }  // Защищаем от создания через new Singleton

    public static function singleton()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

}