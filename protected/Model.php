<?php

namespace App;

abstract class Model
{
    protected static $table = null;

    public $id;

    public static function findAll()
    {
        $sql = 'SELECT * FROM ' . static::$table;
        $db = new Db();

        return $db->query($sql, static::class);
    }

    public static function findById($id)
    {
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE id = :id';
        $db = new Db();
        $param = [':id' => $id];
        $result = $db->query($sql, static::class, $param);
        if (!empty($result)) {

            return $result[0];
        }

        return false;
    }

    public function insert()
    {
        $data = get_object_vars($this);

        $cols = [];
        $binds = [];
        $vals = [];
        foreach ($data as $key => $val) {
            if ('id' == $key) {
                continue;
            }
            $cols[] = $key;
            $binds[] = ':' . $key;
            $vals[':' . $key] = $val;
        }

        $sql = 'INSERT INTO ' . static::$table . ' 
        (' . implode(', ', $cols) . ') 
        VALUES 
        (' . implode(', ', $binds) . ')
        ';
        $db = new Db();
        $db->execute($sql, $vals);
        return $this->id = $db->lastInsertId();
    }

    public function update()
    {
        $data = get_object_vars($this);

        $cols = [];
        $binds = [];
        $vals = [];
        foreach ($data as $key => $val) {
            if ('id' == $key) {
                continue;
            }
            $cols[] = $key;
            $binds[] = ':' . $key;
            $vals[':' . $key] = $val;
        }

        $sql = 'UPDATE ' . static::$table . ' SET  
        (' . implode(', ', $cols) . ') 
        VALUES 
        (' . implode(', ', $binds) . ') WHERE '. static::$table .' id = :id
        ';

        $db = new Db();
        var_dump($sql);
        var_dump($db->execute($sql, $vals));
        return 123;
    }

}