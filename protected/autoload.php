<?php

spl_autoload_register(function ($class)
{
    // App\Models\Product
    require __DIR__ . '/' . str_replace('\\', '/', substr($class, 4)) . '.php';
});