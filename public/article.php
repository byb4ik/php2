<?php

require __DIR__ . '/../protected/autoload.php';

$template =  __DIR__ . '/templates/article_template.php';

$view = new \App\View();

$data = \App\Models\Article::findById($_GET['id']);

$view->assign('article', $data);

$view->display($template);
