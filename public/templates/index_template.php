<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Главная</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link media="screen" href="/public/templates/css/bootstrap.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<?php

foreach ($article as $post): ?>

<h1 align="center"><?php echo $post->title; ?></h1>

<div class="container">
    <p><?php echo $post->text; ?></p>
    <div>
        <a href="/public/article.php?id=<?php echo $post->id ?>">Перейти на страницу новости</a>
<?php endforeach; ?>

</body>
</html>