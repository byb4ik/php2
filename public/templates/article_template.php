<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?php echo $this->data['article']->title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link media="screen" href="/public/templates/css/bootstrap.css" type="text/css" rel="stylesheet" />
</head>

<body>
<h2><a href="/public">Назад</a></h2>
<h1 align="center"><?php echo $this->data['article']->title; ?></h1>

<div class="container">
    <p><?php echo $this->data['article']->text; ?></p>
    </div>

</body>
</html>